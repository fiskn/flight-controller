// Program used to test the USB Joystick library when used as 
// a Flight Controller on the Arduino Leonardo or Arduino 
// Micro.
//
// Matthew Heironimus
// 2016-05-29 - Original Version
//------------------------------------------------------------

#include "Joystick.h"

Joystick_ Joystick(JOYSTICK_DEFAULT_REPORT_ID, 
  JOYSTICK_TYPE_JOYSTICK, 8, 4,
  true, true, false, true, true, false,
  true, true, false, false, false);

#include <Keypad.h>

const byte ROWS = 4; //four rows
const byte COLS = 8; //three columns
char keys[ROWS][COLS] = {
{0,4,8,12,16,20,24,28},
{1,5,9,13,17,21,25,29},
{2,6,10,14,18,22,26,30},
{3,7,11,15,19,23,27,31}
};
byte colPins[COLS] = {0, 1, 6, 7, 2, 3, 4, 5}; //connect to the row pinouts of the kpd
byte rowPins[ROWS] = {10, 15, 14, 16}; //connect to the column pinouts of the kpd

Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );
String msg;


// Set to true to test "Auto Send" mode or false to test "Manual Send" mode.
//const bool testAutoSendMode = true;
const bool testAutoSendMode = false;

const unsigned long gcCycleDelta = 1000;
const unsigned long gcAnalogDelta = 25;
const unsigned long gcButtonDelta = 500;
unsigned long gNextTime = 0;
unsigned int gCurrentStep = 0;

void testSingleButtonPush(unsigned int button)
{
  if (button > 0)
  {
    Joystick.releaseButton(button - 1);
  }
  if (button < 32)
  {
    Joystick.pressButton(button);
  }
}

void testMultiButtonPush(unsigned int currentStep) 
{
  for (int button = 0; button < 32; button++)
  {
    if ((currentStep == 0) || (currentStep == 2))
    {
      if ((button % 2) == 0)
      {
        Joystick.pressButton(button);
      } else if (currentStep != 2)
      {
        Joystick.releaseButton(button);
      }
    } // if ((currentStep == 0) || (currentStep == 2))
    if ((currentStep == 1) || (currentStep == 2))
    {
      if ((button % 2) != 0)
      {
        Joystick.pressButton(button);
      } else if (currentStep != 2)
      {
        Joystick.releaseButton(button);
      }
    } // if ((currentStep == 1) || (currentStep == 2))
    if (currentStep == 3)
    {
      Joystick.releaseButton(button);
    } // if (currentStep == 3)
  } // for (int button = 0; button < 32; button++)
}



void testThrottleRudder(unsigned int value)
{
  Joystick.setThrottle(value);
  Joystick.setRudder(255 - value);
}

void setup() {
  Serial.begin(9600);
  Joystick.setXAxisRange(-511, 511);
  Joystick.setYAxisRange(-511, 511);
  Joystick.setZAxisRange(-511, 511);
  Joystick.setThrottleRange(0, 1023);
  Joystick.setRudderRange(-511, 511);
  Joystick.begin();
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  pinMode(A8, INPUT);
  pinMode(A9, INPUT);
}

void loop() {
  delay(100);
  int xAxis;
  int yAxis;
  int rudder;
  int throttle;
  int r1,r2;
  yAxis=511-analogRead(A0);
  xAxis=511-analogRead(A1);
  rudder=analogRead(A2);
  throttle=analogRead(A3);
  r1=analogRead(A8);
  r2=analogRead(A9);
  Joystick.setXAxis(xAxis);
  Joystick.setYAxis(yAxis);
  Joystick.setRxAxis(r1);
  Joystick.setRyAxis(r2);
  Joystick.setRudder(460-rudder);
  Joystick.setThrottle(throttle);
  if (kpd.getKeys())
    {
        for (int i=0; i<LIST_MAX; i++)   // Scan the whole key list.
        {
            if ( kpd.key[i].stateChanged )   // Only find keys that have changed state.
            {
                switch (kpd.key[i].kstate) {  // Report active key state : IDLE, PRESSED, HOLD, or RELEASED
                    case PRESSED:
                    msg = " PRESSED.";
                    if(kpd.key[i].kchar==8||
                       kpd.key[i].kchar==9||
                       kpd.key[i].kchar==10||
                       kpd.key[i].kchar==11||
                       kpd.key[i].kchar==12||
                       kpd.key[i].kchar==13||
                       kpd.key[i].kchar==14||
                       kpd.key[i].kchar==15||
                       kpd.key[i].kchar==16||
                       kpd.key[i].kchar==17||
                       kpd.key[i].kchar==18||
                       kpd.key[i].kchar==19||
                       kpd.key[i].kchar==24||
                       kpd.key[i].kchar==25||
                       kpd.key[i].kchar==26||
                       kpd.key[i].kchar==27||
                       kpd.key[i].kchar==28||
                       kpd.key[i].kchar==29||
                       kpd.key[i].kchar==30||
                       kpd.key[i].kchar==31)
                    {
                      if(kpd.key[i].kchar==8)
                        Joystick.setHatSwitch(0, 0);
                      if(kpd.key[i].kchar==10)
                        Joystick.setHatSwitch(0, 270);
                      if(kpd.key[i].kchar==11)
                        Joystick.setHatSwitch(0, 90);
                      if(kpd.key[i].kchar==9)
                        Joystick.setHatSwitch(0, 180);
                      if(kpd.key[i].kchar==12)
                        Joystick.setHatSwitch(1, 0);  
                      if(kpd.key[i].kchar==13)
                        Joystick.setHatSwitch(1, 180);
                      if(kpd.key[i].kchar==14)
                        Joystick.setHatSwitch(1, 270);
                      if(kpd.key[i].kchar==15)
                        Joystick.setHatSwitch(1, 90);
                      if(kpd.key[i].kchar==16)
                        Joystick.setHatSwitch(2, 270);  
                      if(kpd.key[i].kchar==17)
                        Joystick.setHatSwitch(2, 0);
                      if(kpd.key[i].kchar==18)
                        Joystick.setHatSwitch(2, 90);
                      if(kpd.key[i].kchar==19)
                        Joystick.setHatSwitch(2, 180);
                      /*if(kpd.key[i].kchar==24)
                        Joystick.setHatSwitch(2, 270);  
                      if(kpd.key[i].kchar==25)
                        Joystick.setHatSwitch(2, 180);
                      if(kpd.key[i].kchar==26)
                        Joystick.setHatSwitch(2, 90);
                      if(kpd.key[i].kchar==27)
                        Joystick.setHatSwitch(2, 0);*/   
                      if(kpd.key[i].kchar==28)
                        Joystick.setHatSwitch(3, 270);  
                      if(kpd.key[i].kchar==29)
                        Joystick.setHatSwitch(3, 0);
                      if(kpd.key[i].kchar==30)
                        Joystick.setHatSwitch(3, 90);
                      if(kpd.key[i].kchar==31)
                        Joystick.setHatSwitch(3, 180);    
                    }
                    else if(kpd.key[i].kchar==20)
                        Joystick.pressButton(1);
                    else if(kpd.key[i].kchar==23)
                        Joystick.pressButton(2);
                    else
                      Joystick.pressButton(kpd.key[i].kchar);
                      
                break;
                    case HOLD:
                    msg = " HOLD.";
                break;
                    case RELEASED:
                    msg = " RELEASED.";
                    if(kpd.key[i].kchar==8||
                       kpd.key[i].kchar==9||
                       kpd.key[i].kchar==10||
                       kpd.key[i].kchar==11||
                       kpd.key[i].kchar==12||
                       kpd.key[i].kchar==13||
                       kpd.key[i].kchar==14||
                       kpd.key[i].kchar==15||
                       kpd.key[i].kchar==16||
                       kpd.key[i].kchar==17||
                       kpd.key[i].kchar==18||
                       kpd.key[i].kchar==19||
                       kpd.key[i].kchar==24||
                       kpd.key[i].kchar==25||
                       kpd.key[i].kchar==26||
                       kpd.key[i].kchar==27||
                       kpd.key[i].kchar==28||
                       kpd.key[i].kchar==29||
                       kpd.key[i].kchar==30||
                       kpd.key[i].kchar==31)
                    {
                      if(kpd.key[i].kchar==8)
                        Joystick.setHatSwitch(0, -1);
                      if(kpd.key[i].kchar==10)
                        Joystick.setHatSwitch(0, -1);
                      if(kpd.key[i].kchar==11)
                        Joystick.setHatSwitch(0, -1);
                      if(kpd.key[i].kchar==9)
                        Joystick.setHatSwitch(0, -1);
                      if(kpd.key[i].kchar==12)
                        Joystick.setHatSwitch(1, -1);  
                      if(kpd.key[i].kchar==13)
                        Joystick.setHatSwitch(1, -1);
                      if(kpd.key[i].kchar==14)
                        Joystick.setHatSwitch(1, -1);
                      if(kpd.key[i].kchar==15)
                        Joystick.setHatSwitch(1, -1);
                      if(kpd.key[i].kchar==16)
                        Joystick.setHatSwitch(2, -1);  
                      if(kpd.key[i].kchar==17)
                        Joystick.setHatSwitch(2, -1);
                      if(kpd.key[i].kchar==18)
                        Joystick.setHatSwitch(2, -1);
                      if(kpd.key[i].kchar==19)
                        Joystick.setHatSwitch(2, -1);
                      /*if(kpd.key[i].kchar==24)
                        Joystick.setHatSwitch(2, -1);  
                      if(kpd.key[i].kchar==25)
                        Joystick.setHatSwitch(2, -1);
                      if(kpd.key[i].kchar==26)
                        Joystick.setHatSwitch(2, -1);
                      if(kpd.key[i].kchar==27)
                        Joystick.setHatSwitch(2, -1);*/
                      if(kpd.key[i].kchar==28)
                        Joystick.setHatSwitch(3, -1);  
                      if(kpd.key[i].kchar==29)
                        Joystick.setHatSwitch(3, -1);
                      if(kpd.key[i].kchar==30)
                        Joystick.setHatSwitch(3, -1);
                      if(kpd.key[i].kchar==31)
                        Joystick.setHatSwitch(3, -1);    
                    }
                    else if(kpd.key[i].kchar==20)
                        Joystick.releaseButton(1);
                    else if(kpd.key[i].kchar==23)
                        Joystick.releaseButton(2);
                    else
                      Joystick.releaseButton(kpd.key[i].kchar);
                      
                break;
                    case IDLE:
                    msg = " IDLE.";
                }
                Serial.print("Key ");
                Serial.print(kpd.key[i].kchar);
                Serial.println(msg);
            }
        }
    }
}
